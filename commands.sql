/*134 2*/
ALTER TABLE progress
    ADD COLUMN test_form text NOT NULL,
    ADD CHECK (test_form in ('экзамен', 'зачет'));

alter table progress
    add check (
        (test_form='экзамен' AND mark in (3,4,5))
        or
        (test_form='зачет' and mark in (0,1))
    );

INSERT INTO students ( record_book, name, doc_ser, doc_num )
    VALUES  ( 12300, 'Иванов Иван Иванович', 0402, 543281 ),
            ( 12301, 'Петров Тарам', 0403, 543282 ),
            ( 12302, 'Мухожуйко Марат', 0404, 543283 );

alter table progress
    alter column mark set default 6;

INSERT INTO progress ( record_book, subject, acad_year, term )
    VALUES ( 12300, 'Физика', '2016/2017',1 );
