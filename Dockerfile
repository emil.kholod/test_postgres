FROM postgres:9.6
RUN apt-get update && apt-get install -y \
    vim\
    wget\
    unzip
RUN mkdir mounted_db
WORKDIR mounted_db
COPY . /mounted_db
RUN wget https://edu.postgrespro.ru/demo-small.zip
RUN unzip demo-small.zip
CMD ["postgres"]
